/**
 * Given a list of teams, we need to expose the teams in different ways:
 * - Criteria 1: depending on the score, order the list from highest to lowest.
 * - Criteria 2: depending on the score, order the list from lowest to highest.
 * - Criteria 3: depending on the player's quantity, show ONLY the teams that has more than 3 players.
 *
 * What you need to implement is:
 * - 3 buttons. Each of them need to execute a criteria.
 * - The list of teams should be updated dynamically depending on the selected filter.
 * - Each team item should display: Team Name / Player’s quantity / Total Score of each team.
 */

import { useEffect, useState } from "react";

const TEAMS = [
  {
    name: "Red",
    players: ["Robin", "Rey", "Roger", "Richard"],
    games: [
      {
        score: 10,
        city: "LA",
      },
      {
        score: 1,
        city: "NJ",
      },
      {
        score: 3,
        city: "NY",
      },
    ],
  },
  {
    name: "Blue",
    players: ["Bob", "Ben"],
    games: [
      {
        score: 6,
        city: "CA",
      },
      {
        score: 3,
        city: "LA",
      },
    ],
  },
  {
    name: "Yellow",
    players: ["Yesmin", "Yuliana", "Yosemite"],
    games: [
      {
        score: 2,
        city: "NY",
      },
      {
        score: 4,
        city: "LA",
      },
      {
        score: 7,
        city: "AK",
      },
    ],
  },
];

export function TeamsList() {
  let initialState = setInitialTeams(TEAMS);
  const [teams, setTeams] = useState(initialState);
  const [higherToLower, setHigherToLower] = useState(false);
  const [lowerToHigher, setLowerToHigher] = useState(false);
  const [teamPlayer, setTeamPlayer] = useState(false);

  // Order teams by score (highest to lowest)
  const orderTeamByScoreHighestToLowest = (value) => {
    let newState = value.sort((a, b) => b.score - a.score);
    return setTeams(newState);
  };

  // Order teams by score (lowest to highest)
  function orderTeamByScoreLowestToHighest(value) {
    let newState = value.sort((a, b) => a.score - b.score);
    return setTeams(newState);
  }

  // Filtering teams that with at least 3 players
  function teamsWithMoreThanThreePlayers(value) {
    let newState = value.filter((minPlayers) => 3 < minPlayers.players);
    return setTeams(newState);
  }

  useEffect(() => {
    if (teamPlayer) {
      setTeamPlayer(false);
      teamsWithMoreThanThreePlayers(initialState);
    }
    if (lowerToHigher) {
      setLowerToHigher(false);
      orderTeamByScoreLowestToHighest(initialState);
    }
    if (higherToLower) {
      setHigherToLower(false);
      orderTeamByScoreHighestToLowest(initialState);
    }
  }, [teamPlayer, lowerToHigher, higherToLower, initialState]);

  return (
    <div>
      <button onClick={() => setTeams(initialState)}>Initial list</button>

      <button onClick={() => setHigherToLower(true)}>Highest to Lowest</button>
      <button onClick={() => setLowerToHigher(true)}>Lowest to Highest</button>
      <button onClick={() => setTeamPlayer(true)}>
        Teams with at least 3 players
      </button>

      <ul className="teams">
        {teams.map((team, i) => (
          <li key={i}>
            Name: {team.name} | Players: {team.players} | Score: {team.score}
          </li>
        ))}
      </ul>
    </div>
  );
}

function setInitialTeams(value) {
  let initialState = [];
  value.length &&
    value.map((team) =>
      initialState.push({
        name: team.name,
        players: team.players.length,
        score: team.games
          .map((city) => city.score)
          .reduce((accumulator, currentValue) => (accumulator += currentValue)),
      })
    );
  return initialState;
}
