/**
 * Implement the ImageGallery component that accepts a `links`
 * prop and renders the gallery so that the first
 * item in the links prop is the src attribute of the first image in the gallery.

 * It should also implement the following logic:
 * - When the button is clicked, the image that is in the same div as the button should be removed from the gallery.
 */

import { useState } from "react";

function Image({ src, onRemove }) {
  const handleClick = () => onRemove(src);
  return (
    <div className="image">
      <img src={src} alt={src} />
      <button onClick={() => handleClick()} className="remove">
        X
      </button>
    </div>
  );
}

export function ImageGallery({ links }) {
  const [gallery, setGalerry] = useState(links);
  const onRemove = (link) =>
    setGalerry(gallery.filter((filter) => filter !== link));
  return (
    <div>
      {gallery.map((link, i) => (
        <Image key={i} src={link} onRemove={onRemove} />
      ))}
    </div>
  );
}
