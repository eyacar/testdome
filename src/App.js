
import { Message } from "./components/Message";
import { FocusableInput } from "./components/FocusableInput";
import { ImageGallery } from "./components/ImageGallery";
import { PlayerStatus } from "./components/PlayerStatus";
import { TeamsList } from "./components/TeamsList";
import { Grocery } from "./components/Grocery";

import './App.css';

const grocery = [{ name: "Oranges", votes: 0 }, { name: "Bananas", votes: 0 }]
const image = ["https://via.placeholder.com/150/FF0000/FFFFFF/?text=TEST","https://via.placeholder.com/150/FFFF00/000000/?text=TEST","https://via.placeholder.com/150/0000FF/808080/?text=TEST"]

export default function App() {
  return (
    <div className="App">
      {/* Render here each component from the "components" directory */}
      <h3>'Message' test</h3>
      <Message />
      <br />
      <h3>'Grocery' test</h3>
      <Grocery products={grocery} />
      <br />
      <h3>'FocusableInput' test</h3>
      <FocusableInput focusable={true}/>
      <br />
      <h3>'ImageGallery' test</h3>
      <ImageGallery links={image}/>
      <br />
      <h3>'PlayerStatus' test</h3>
      <PlayerStatus />
      <br />
      <h3>'TeamsList' test</h3>
      <TeamsList />
    </div>
  );
}
